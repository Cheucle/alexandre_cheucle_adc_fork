/*----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support  -  ROUSSET  -
 *----------------------------------------------------------------------------
 * The software is delivered "AS IS" without warranty or condition of any
 * kind, either express, implied or statutory. This includes without
 * limitation any warranty or condition with respect to merchantability or
 * fitness for any particular purpose, or against the infringements of
 * intellectual property rights of others.
 *----------------------------------------------------------------------------
 * File Name           : features_sama5xxxxx.h
 * Object              : ATSAM Definition File.
 *
 * Creation            : Sept/2011
 *----------------------------------------------------------------------------
 */
#ifndef _FEATURES_SAMA5D27_H
#define _FEATURES_SAMA5D27_H

// Add here features definitions that are specific to a particular CPN (not common to series)

#endif //_FEATURES_SAMA5D27_H
